package soapQueue;
// Implementa e pubblica il servizio

import javax.xml.ws.Endpoint;

public class Server {
	public static void main(String[ ] args) {
//Se non c'è un parametro invoca su localhost
		String IP = args.length>0?args[0]:"127.0.0.1";
		try {
			Endpoint.publish("http://"+IP+":9876/webserver", new WebQueue());
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		System.out.println("WebQueue pronto alla porta 9876");
	}
}

