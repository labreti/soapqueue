package soapQueue;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
abstract interface WebQueueInterface {
	@WebMethod void add (String s);
	@WebMethod String poll();
}
