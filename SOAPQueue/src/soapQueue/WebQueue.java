package soapQueue;
// Definisce l'implementazione del servizio

import java.util.concurrent.ConcurrentLinkedQueue;

import javax.jws.WebService;

@WebService(endpointInterface = "soapQueue.WebQueueInterface")
public class WebQueue implements WebQueueInterface {
	private ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<String>();
    public void add(String s) { queue.add(s); }
	public String poll() { String s=queue.poll(); return s; }
}
