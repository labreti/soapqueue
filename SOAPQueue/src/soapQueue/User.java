package soapQueue;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

	public class User {
		public static void main(String args[ ]) throws Exception {
			//Se non c'è un parametro invoca su localhost
					String IP = args.length>0?args[0]:"127.0.0.1";
			// Ottengo la visibilità del servizio
			Service service = Service.create(
					// La URL del documento che definisce il servizio
					            new URL("http://"+IP+":9876/webserver?wsdl"),
					// Il Namespace e la classe nel Namespace a cui si fa riferimento
					            new QName("http://soapQueue/", "WebQueueService"));
			// Estraggo la ''endpoint interface'' del servizio
			WebQueueInterface eif = service.getPort(WebQueueInterface.class);

			eif.add("uno");
			eif.add("due");
			System.out.println(eif.poll());
		}
	}
